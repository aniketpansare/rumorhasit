package com.ap.rumorhasit.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ap.rumorhasit.R;

import java.util.List;

/**
 * Created by aniket on 3/21/14.
 */
public class RumorArrayAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final List<String> values;

    /**
     * Constructor.
     * @param context
     * @param values
     */
    public RumorArrayAdapter(Context context,List<String> values)
    {
        super(context, R.layout.rumor_row_layout,values);
        this.context=context;
        this.values=values;
    }

    /**
     * Override getView Method.
     * Called when array adapter initialized.
     * Called once for each element in the list.
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rumor_row_layout,parent,false);
        TextView textView = (TextView) rowView.findViewById(R.id.rumor_text);

        textView.setText(values.get(position));

        return rowView;
    }
}
