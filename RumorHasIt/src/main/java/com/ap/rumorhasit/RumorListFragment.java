package com.ap.rumorhasit;

import android.os.Bundle;
import android.support.v4.app.ListFragment;

import com.ap.rumorhasit.domain.RumorContent;
import com.ap.rumorhasit.dummy.DummyContent;
import com.ap.rumorhasit.util.RumorArrayAdapter;

/**
 * A fragment representing a single Group detail screen.
 * This fragment is either contained in a {@link GroupListActivity}
 * in two-pane mode (on tablets) or a {@link RumorListActivity}
 * on handsets.
 */
public class RumorListFragment extends ListFragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private DummyContent.DummyItem mItem;

    private RumorArrayAdapter listAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RumorListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
        }


        // TODO: replace with a real list adapter.
        //Initialize Custom Array Adapter with chat history.
        listAdapter = new RumorArrayAdapter(getActivity().getApplicationContext(), RumorContent.RUMORS_STRING_LIST);
        setListAdapter(listAdapter);
    }
/*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_group_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
          ((TextView) rootView.findViewById(R.id.group_detail)).setText(mItem.content);
        }

        return rootView;
    }
    */
}
