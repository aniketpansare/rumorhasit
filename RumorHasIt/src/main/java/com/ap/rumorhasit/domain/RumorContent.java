package com.ap.rumorhasit.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by aniket on 3/21/14.
 */
public class RumorContent {

    /**
     * An array of sample (dummy) items.
     */
    public static List<RumorItem> RUMORS_LIST = new ArrayList<RumorItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<String, RumorItem> RUMORS_MAP = new HashMap<String, RumorItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static List<String> RUMORS_STRING_LIST =new ArrayList<String>();

    static {
        // Add 3 sample items.
        addItem(new RumorItem("1", "CollegeRumor: CS598 There is going to be a surprise test tomorrow"));
        addItem(new RumorItem("2", "CelebrityRumor: Justin beiber is cheating on Selena"));
        addItem(new RumorItem("3", "Sports: 'Moyes' going to be sacked at the end of season"));

    }

    private static void addItem(RumorItem item) {
        RUMORS_LIST.add(item);
        RUMORS_MAP.put(item.id, item);
        RUMORS_STRING_LIST.add(item.content);
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class RumorItem {
        public String id;
        public String content;

        public RumorItem(String id, String content) {
            this.id = id;
            this.content = content;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
